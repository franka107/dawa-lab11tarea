const jwt = require("jsonwebtoken");

let verificarToken = (req, res, next) => {
  let token = req.cookies.token;
  jwt.verify(token, "seed-desarrollo", (err, decoded) => {
    if (err) {
      return res.send("verificacion erroneea");
    }

    req.usuario = decoded.usuario;
    console.log(req.usuario);
    next();
  });
};

module.exports = {
  verificarToken,
};
